#!/usr/bin/python
# -*- coding: utf-8 -*-

from selenium.webdriver.common.by import By


class PlatbaNaMiruLink11:

    def __init__(self, driver):
        self.driver = driver

    payment_link = (By.TAG_NAME, "a")

    def select_test(self):
        return self.driver.find_element(*PlatbaNaMiruLink11.payment_link)
