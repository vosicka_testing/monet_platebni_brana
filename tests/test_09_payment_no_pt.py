#!/usr/bin/python
# -*- coding: utf-8 -*-

from utilities.BaseClass import BaseClass
from pageObjects.Payment_no_pt_09 import PaymentNoPt09
from TestData.Brana_test_data import HomePageData
import time
import pytest

class Test09(BaseClass):

    def test_standard_payment(self, getData):
        # Standardní platba: karta + mpass@bank, bez pt@bank
        tc09 = PaymentNoPt09(self.driver)
        # logger
        log = self.getLogger()
        tc09.select_test().click()
        tc09.confirm_test().click()
        self.explicit_wait_function_ID('creditcard')
        payment_buttons = tc09.payment_methods()
        payments = ['button-pay-mpass', 'button-pay-csob', 'button-pay-era']
        try:
            assert len(payment_buttons) == 1
            assert payment_buttons[0].get_attribute("class") == payments[0]
            log.info("Dostupne pouze tlacitko masterpass - podle scenare OK")
            not_found = True
        except:
            log.warning("Dostupne vice platebnich metod - NOK")
            not_found = False
        assert not_found
        tc09.fill_card_number().send_keys(getData["card_number"])
        time.sleep(1)
        tc09.click_show_popup_error().click()
        tc09.fill_expiration().send_keys(getData["expiration"])
        tc09.fill_cvc().send_keys(getData["cvc"])
        log.info("Card number: " + getData["card_number"] + " Expiration: " + getData["expiration"]+" CVC: " +
                 getData["cvc"])
        tc09.confirm_payment_form().submit()
        self.explicit_wait_function_text('//strong[text()="4 (APPROVED)"]')
        self.redirect_to_start(self.env_path)

    @pytest.fixture(params=HomePageData.test_09_payment_no_pt)
    # @pytest.fixture(params=HomePageData.getTestData('TestCase1'))
    def getData(self, request):
        return request.param