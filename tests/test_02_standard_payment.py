#!/usr/bin/python
# -*- coding: utf-8 -*-

from utilities.BaseClass import BaseClass
from pageObjects.Standard_payment_02 import StandardPayment02
from TestData.Brana_test_data import HomePageData
import time
import pytest

class Test02(BaseClass):

    def test_standard_payment(self, getData):
        # definování prvního testu: Standardní platba s možností uložit kartu
        tc02 = StandardPayment02(self.driver)
        # logger
        log = self.getLogger()
        tc02.select_test().click()
        tc02.confirm_test().click()
        self.explicit_wait_function_ID('creditcard')
        payment_buttons = tc02.payment_methods()
        payments = ['button-pay-mpass', 'button-pay-csob', 'button-pay-era']
        for pb in payment_buttons:
            assert pb.get_attribute("class") in payments
        assert len(payment_buttons) == 3
        tc02.fill_card_number().send_keys(getData["card_number"])
        time.sleep(1)
        tc02.fill_expiration().send_keys(getData["expiration"])
        tc02.fill_cvc().send_keys(getData["cvc"])
        try:
            tc02.save_card_check().click()
            not_found = False
        except:
            log.warning("Platba bez moznosti ulozit kartu - podle scenare OK")
            not_found = True
        assert not_found
        log.info("Card number: " + getData["card_number"] + " Expiration: " + getData["expiration"]+" CVC: " +
                 getData["cvc"])
        tc02.confirm_payment_form().submit()
        self.explicit_wait_function_text('//strong[text()="4 (APPROVED)"]')
        self.redirect_to_start(self.env_path)

    @pytest.fixture(params=HomePageData.test_02_standard_payment)
    # @pytest.fixture(params=HomePageData.getTestData('TestCase1'))
    def getData(self, request):
        return request.param